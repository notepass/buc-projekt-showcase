#!/usr/bin/env bash
if ! [ -d ./docker-builds-external/conductor ]
then
echo "Cloning needed repos" && \
git clone "https://github.com/Netflix/conductor.git" ./docker-builds-external/conductor
fi

echo "Building maven projects" && \
docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.6.1-jdk-8-alpine mvn clean install && \
\
echo "Building docker containers" && \
docker-compose build && \
\
echo "Done. Start with: docker-compose up"