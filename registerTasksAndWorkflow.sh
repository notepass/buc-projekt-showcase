#!/usr/bin/env bash
CONDUCTOR_API_URL="http://192.168.99.100:8080/api"

for f in ./microservices/*/src/main/resources/taskDefinition.yaml
do
  echo "Registering task $f"
  curl -X POST "$CONDUCTOR_API_URL/metadata/taskdefs" -H 'Content-Type: application/json' -d "[$(cat "$f")]"
done

curl -X PUT "$CONDUCTOR_API_URL/metadata/workflow" -H 'Content-Type: application/json' -d "[$(cat ./microservices/workflow-definition.yaml)]"
curl -X POST "$CONDUCTOR_API_URL/metadata/workflow" -H 'Content-Type: application/json' -d "$(cat ./microservices/workflow-definition.yaml)"

curl -X POST "$CONDUCTOR_API_URL/workflow/linear_shop_test" -H 'Content-Type: application/json'   -d '{
"userId": 1,
"productId": 1,
"productAmount": 3
}'