import React from "react";
import { Redirect, Route } from "react-router";
import authService from "../services/auth/auth-service";

//@ts-ignore
export const ProtectedRoute = ({ component: Component, ...rest }) => {
	return (
		<Route
			{...rest}
			render={(props: any) => {
				if (authService.isLoggedIn()) {
					return <Component {...props} />;
				} else {
					return (
						<Redirect
							to={{
								pathname: "/login",
								state: { from: props.location }
							}}
						/>
					);
				}
			}}
		/>
	);
};
