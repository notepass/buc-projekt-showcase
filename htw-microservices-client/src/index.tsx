import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "./styles.scss";
import { Provider } from "mobx-react";
import initializeStores from "./stores/store-initializer";
import { BrowserRouter } from "react-router-dom";

const stores = initializeStores();
console.log(stores);

stores.authStore.initialize().then(() =>
	ReactDOM.render(
		<Provider {...stores}>
			<BrowserRouter>
				<App />
			</BrowserRouter>
		</Provider>,
		document.getElementById("root")
	)
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
