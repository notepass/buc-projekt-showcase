import React from "react";
import "./App.css";
import Shop from "./components/shop/shop";
import Login from "./components/login/login";
import AuthStore from "./stores/auth/authStore/auth-store";
import { observer, inject } from "mobx-react";
import Stores from "./stores/store-identifier";
import { Switch, Route } from "react-router";
import { ProtectedRoute } from "./utilities/ProtectedRoute";

export interface IAppProps {
	authStore?: AuthStore;
}

@inject(Stores.AuthStore)
@observer
class App extends React.Component<IAppProps, any> {
	render() {
		console.log(
			"rendering app, customer is",
			this.props.authStore!.loginState
		);
		return (
			<div className="App">
				<Switch>
					<ProtectedRoute path="/" exact component={Shop} />
					<Route path="/login" component={Login} />
				</Switch>
			</div>
		);
	}
}

export default App;
