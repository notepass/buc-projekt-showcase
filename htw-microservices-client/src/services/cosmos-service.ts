import { CosmosClient, Container } from '@azure/cosmos';

class CosmosService {

    private endpoint: any = "https://htw-microservices-db.documents.azure.com:443";
    private masterKey: any = "olVR7aMpk9XRbnLsAAEQGRjrA6xqiq2Y67VMhKMelxBLEbBfEptSIZLWpqc2sGam91OcyhyZX7qoY21uIdjoXg==";

    public client = new CosmosClient({ endpoint: this.endpoint, auth: { key: this.masterKey } });    

    private dbContainer: Container | undefined = undefined;

    async initialize() {
        const db = await this.client.database("OrderSystem");
        console.log('connected to db');

        this.dbContainer = await db.container("Items");
        console.log('connected to container');
        
    }

    public async container(): Promise<Container> {
        if (this.dbContainer === undefined) {
            await this.initialize();
        }
        return this.dbContainer!;    
    }

}

export default new CosmosService();