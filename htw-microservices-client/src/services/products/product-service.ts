import cosmosService from "../cosmos-service";
import { Product } from "../../model/product";

class ProductService {
    public async getAllProducts() {
        const getAllProductsQuery = {
            query: "SELECT * FROM Items i WHERE i.entityType = 'Product'",
            parameters: []
        };

        const results = await (await cosmosService.container()).items.query(getAllProductsQuery).toArray();

        var products: Product[] = [];

        for (var queryResult of results.result!) {
            products.push(queryResult);
        }

        return products;
    }
}

export default new ProductService();