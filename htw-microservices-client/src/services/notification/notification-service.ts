import authService from "../auth/auth-service";
import cosmosService from "../cosmos-service";
import { Notification } from "../../model/notification";

class NotificationService{

    public async getAllNotifications() {
        const getMyNotificationsQuery = {
            query: "SELECT * FROM Items i WHERE i.entityType = 'Notification' and i.customerId = @customerId",
            parameters: [
                {
                    name: "@customerId",
                    value: authService.getLocalCustomerId()!
                }
            ]
        };

        const results = await (await cosmosService.container()).items.query(getMyNotificationsQuery).toArray();

        var notifications: Notification[] = [];

        for (var queryResult of results.result!) {
            notifications.push(queryResult);
        }

        return notifications;
    }

}

export default new NotificationService();