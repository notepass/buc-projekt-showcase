import cosmosService from "../cosmos-service";
import LoginInput from "./login-input";
import { Customer } from "../../model/customer";

class AuthService {

    customerIdKey: string = "customerId";

    public isLoggedIn(): boolean {
        return localStorage.getItem(this.customerIdKey) != null;
    }

    public async logIn(loginInput: LoginInput): Promise<Customer | undefined> {
        const loginQuery = {
            query: "SELECT * FROM Items i WHERE i.entityType = 'Customer' AND i.username = @username AND i.password = @password",
            parameters: [
                {
                    name: "@username",
                    value: loginInput.username
                },
                {
                    name: "@password",
                    value: loginInput.password
                }
            ]
        };

        const results = await (await cosmosService.container()).items.query(loginQuery).toArray();
        console.log(results);

        if (results.result!.length > 0) {
            localStorage.setItem(this.customerIdKey, results.result![0].id);
            return results.result![0];
        }
        return undefined;
    };

    public getLocalCustomerId(){
        if(!this.isLoggedIn) return;

        return localStorage.getItem(this.customerIdKey);
    }

    public async getLocalCustomer() {
        if (!this.isLoggedIn) return;

        const getCustomerQuery = {
            query: "SELECT * FROM Items i WHERE i.entityType = 'Customer' AND i.id = @id",
            parameters: [
                {
                    name: "@id",
                    value: localStorage.getItem(this.customerIdKey)!
                }
            ]
        };

        const results = await (await cosmosService.container()).items.query(getCustomerQuery).toArray();
        console.log(results);

        if (results.result!.length > 0) {            
            return results.result![0];
        }
        return undefined;
    }

    public logOut() {
        localStorage.removeItem(this.customerIdKey);
    }
}

export default new AuthService();