import CartItem from "../../model/cartItem";
import { Order } from "../../model/order";
import authService from "../auth/auth-service";
import axios from "axios";

class OrderService {

    private funcEndpoint = "https://htw-microservices.azurewebsites.net/api/OrderOrchestrator_HttpStart";

    public sendOrder(cartItems: CartItem[]) {
        var order = new Order();
        order.customerId = authService.getLocalCustomerId()!;

        cartItems.forEach((i, x) =>
            order.orderLines.push({ lineNo: x * 10, productId: i.product.id, entityType: "OrderLine", amount: i.amount })
        )
        console.log(order);
        axios.post(this.funcEndpoint, order);
    }
}

export default new OrderService();