import { IEntity } from "./IEntity";

export class Order implements IEntity {
    id: string = ""
    orderNo: string = ""
    customerId: string = ""
    orderDate: string = new Date().toISOString()
    orderStatus: string = "New"
    orderLines: OrderLine[] = []
    shipment: any = null
    entityType: string = "Order"
}

export class OrderLine {
    lineNo: number = 0
    productId: string = ""
    amount: number = 0
    entityType: string = "OrderLine"
}