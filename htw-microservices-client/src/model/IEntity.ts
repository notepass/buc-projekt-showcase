export interface IEntity {
    id: string;
    entityType: string;
}