import { Customer } from "./customer";

export class LoginState {
    isLoggedIn: boolean = false;
    customer: Customer = new Customer();
}