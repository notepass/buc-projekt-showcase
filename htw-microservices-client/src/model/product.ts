import { IEntity } from "./IEntity";

export interface Product extends IEntity {
    name: string;
    description: string;
    price: number;
    stockAmount: number;
}