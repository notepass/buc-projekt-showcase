import { IEntity } from "./IEntity";

export class Customer implements IEntity {
    firstName: string = "";
    lastName: string = "";
    email: string = "";
    username: string = "";
    password: string = "";
    id: string = "";
    entityType: string = "Customer";
}