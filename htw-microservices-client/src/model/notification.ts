import { IEntity } from "./IEntity";

export class Notification implements IEntity {
    customerId: string = ""
    title: string = ""
    body: string = ""
    isRead: boolean = false
    date: Date = new Date()
    id: string = ""
    entityType: string = "Notification"
}