import { observable, action } from "mobx";
import notificationService from "../../services/notification/notification-service";
import { Notification } from "../../model/notification";

class NotificationStore {

    @observable notifications: Notification[] = [];

    @action async getAllNotifications() {
        this.notifications =  await notificationService.getAllNotifications();        
    }
}

export default NotificationStore;