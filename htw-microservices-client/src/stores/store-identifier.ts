export default class Stores {
  static AuthStore: string = 'authStore';
  static OrderStore: string = 'orderStore';
  static CustomerStore: string = 'customerStore';
  static ProductStore: string = 'productStore';
  static NotificationStore: string = 'notificationStore';
  static CartStore: string = 'cartStore';
}
