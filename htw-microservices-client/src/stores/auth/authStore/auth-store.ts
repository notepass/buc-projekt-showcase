import { observable, action } from "mobx";
import LoginInput from "../../../services/auth/login-input";
import authService from "../../../services/auth/auth-service";
import { LoginState } from "../../../model/loginState";
import { Customer } from "../../../model/customer";

class AuthStore {
  @observable loginState: LoginState = new LoginState();

 public async initialize() {
    if (authService.isLoggedIn()) {
      var localCust = await authService.getLocalCustomer()
      console.log("already logged in", localCust);
      this.loginState.customer = localCust;
      this.loginState.isLoggedIn = true;
    }
  }

  @action
  async logIn(loginInput: LoginInput): Promise<boolean> {
    var loggedInCustomer = await authService.logIn(loginInput);

    if (loggedInCustomer !== undefined) {
      this.loginState.customer = loggedInCustomer;
      this.loginState.isLoggedIn = true;
      console.log("logged in, customer is", loggedInCustomer)
      return true;
    }
    return false;
  }

  @action
  logOut() {
    authService.logOut();
    this.loginState.customer = new Customer();
    this.loginState.isLoggedIn = false;
  }
}

export default AuthStore;