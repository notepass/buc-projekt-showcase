import AuthStore from "./auth/authStore/auth-store";
import OrderStore from "./order/order-store";
import CustomerStore from "./customer/customer-store";
import ProductStore from "./product/product-store";
import NotificationStore from "./notification/notification-store";
import CartStore from "./cart/cart-store";

export default function initializeStores() {
  return {
    authStore: new AuthStore(),
    orderStore: new OrderStore(),
    customerStore: new CustomerStore(),
    productStore: new ProductStore(),
    notificationStore: new NotificationStore(),
    cartStore: new CartStore()
  };
}