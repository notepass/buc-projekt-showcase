import { action, observable } from "mobx";
import CartItem from "../../model/cartItem";

class CartStore {
    @observable items: CartItem[] = [];

    @action
    public addItem(cartItem: CartItem) {
        this.items.push(cartItem);
    }

    @action
    public clearCart() {
        this.items = [];
    }

    public getTotal() {
        var total = 0;
        this.items.forEach((i) => total += i.amount* i.product.price);
        return total;
    }
}

export default CartStore;