import { observable, action } from "mobx";
import { Product } from "../../model/product";
import productService from "../../services/products/product-service";

class ProductStore {
/**
 *
 */
constructor() {
    console.log("new Product Store created", this);    
}

    @observable products: Product[] = [];

    @action public async getAllProducts() {
        this.products = await productService.getAllProducts();
    }
}

export default ProductStore;