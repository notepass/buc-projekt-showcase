import * as React from "react";
import "./login.scss";
import AuthStore from "../../stores/auth/authStore/auth-store";
import { inject } from "mobx-react";
import Stores from "../../stores/store-identifier";
import { ReactComponent as Logo } from "../../logo.svg";

export interface ILoginProps {
	authStore?: AuthStore;
}

export interface ILoginState {
	loginSuccess: undefined;
	username: string;
	password: string;
}

@inject(Stores.AuthStore)
class Login extends React.Component<ILoginProps, ILoginState> {
	state = {
		loginSuccess: undefined,
		username: "",
		password: ""
	};

	/**
	 *
	 */
	constructor(props: ILoginProps) {
		super(props);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.onSubmitLogin = this.onSubmitLogin.bind(this);
	}

	private async onSubmitLogin(e: any) {
		e.preventDefault();
		await this.props.authStore!.logIn({
			username: this.state.username,
			password: this.state.password
		});
	}

	handleInputChange(event: any) {
		const target = event.target;
		const value =
			target.type === "checkbox" ? target.checked : target.value;
		const name: string = target.name;

		//@ts-ignore
		this.setState({ [name]: value });
	}

	render() {
		return (
			<div className="app-login text-center">
				<form className="form-signin" onSubmit={this.onSubmitLogin}>
					<Logo className="logo" />
					<h1 className="h3 mb-3 font-weight-normal">
						Bitte anmelden:
					</h1>
					<label className="sr-only">Benutzername</label>
					<input
						type="text"
						id="inputEmail"
						name="username"
						className="form-control"
						placeholder="Benutzername"
						required
						value={this.state.username}
						onChange={this.handleInputChange}
					/>
					<label className="sr-only"> Passwort</label>
					<input
						type="password"
						id="inputPassword"
						name="password"
						className="form-control"
						placeholder="Passwort"
						required
						value={this.state.password}
						onChange={this.handleInputChange}
					/>
					<button
						className="btn btn-lg btn-primary btn-block"
						type="submit">
						Anmelden
					</button>
					<p className="mt-5 mb-3 text-muted">
						&copy; 2019 Florian Hiensch
					</p>
				</form>
			</div>
		);
	}
}

export default Login;
