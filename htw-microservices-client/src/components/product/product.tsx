import * as React from "react";
import "./product.scss";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import { Product } from "../../model/product";
import CartStore from "../../stores/cart/cart-store";
import { inject } from "mobx-react";
import Stores from "../../stores/store-identifier";

export interface IProductProps {
	product: Product;
	cartStore?: CartStore;
}

export interface IProductState {
	amount: number;
}

@inject(Stores.CartStore)
class ProductComp extends React.Component<IProductProps, IProductState> {
	state = { amount: 0 };

	constructor(props: IProductProps) {
		super(props);
		this.handleOnAdd = this.handleOnAdd.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
	}

	private async handleOnAdd() {
		await this.props.cartStore!.addItem({
			product: this.props.product,
			amount: this.state.amount
		});
	}

	handleInputChange(event: any) {
		const target = event.target;
		const value =
			target.type === "checkbox" ? target.checked : target.value;
		const name: string = target.name;

		//@ts-ignore
		this.setState({ [name]: value });
	}

	render() {
		const { product } = this.props;
		return (
			<div className="app-product col-lg-4 col-md-6 col-sm-12">
				<div className="card mb-4 shadow-sm">
					<div className="card-header">
						<h4 className="my-0 font-weight-normal">
							{product.name}
						</h4>
					</div>
					<div className="card-body">
						<h1 className="card-title pricing-card-title">
							{product.price.toLocaleString("de-DE")} €{" "}
							<small className="text-muted">/ Stk.</small>
						</h1>
						<p className="mt-3 mb-4">{product.description}</p>
						<Form.Group controlId="validationCustomUsername">
							{/* <Form.Label>Menge</Form.Label> */}
							<InputGroup>
								<Form.Control
									type="number"
									placeholder="Gewünschte Anzahl"
									name="amount"
									max={this.props.product.stockAmount}
									aria-describedby="inputGroupPrepend"
									onChange={this.handleInputChange}
									value={
										this.state.amount !== 0
											? this.state.amount.toString()
											: ""
									}
									required
								/>
								<Form.Control.Feedback type="invalid">
									Bitte wählen Sie eine gültige Menge
								</Form.Control.Feedback>
								{/* <InputGroup.Append>
									<InputGroup id="inputGroupPrepend">
									</InputGroup>
								</InputGroup.Append> */}
							</InputGroup>
							<Button
								variant="primary"
								onClick={this.handleOnAdd}>
								Zur Bestellung hinzufügen
							</Button>
						</Form.Group>
					</div>
				</div>
			</div>
		);
	}
}

export default ProductComp;
