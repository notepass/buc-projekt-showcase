import * as React from "react";
import "./shop.scss";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import { observer, inject } from "mobx-react";
import Stores from "../../stores/store-identifier";
import AuthStore from "../../stores/auth/authStore/auth-store";
import ProductStore from "../../stores/product/product-store";
import ProductComp from "../product/product";
import { ReactComponent as Logo } from "../../logo.svg";
import CartStore from "../../stores/cart/cart-store";
import orderService from "../../services/order/order-service";
import Notifications from "../notifications/notifications";

export interface IShopProps {
	authStore?: AuthStore;
	productStore?: ProductStore;
	cartStore?: CartStore;
}

export interface IShopState {}

@inject(Stores.AuthStore)
@inject(Stores.ProductStore)
@inject(Stores.CartStore)
@observer
class Shop extends React.Component<IShopProps, IShopState> {
	async componentDidMount() {
		this.props.productStore!.getAllProducts();
	}

	private getGreeting() {
		var time = new Date().getHours();
		if (time <= 4 || time >= 18) {
			//return "Gute Nacht";
			return "Guten Abend";
		} else if (time <= 12) {
			return "Guten Morgen";
		} else if (time <= 18) {
			return "Guten Tag";
		}
		// Default, just in case
		return "Willkommen zurück";
	}

	render() {
		const products =
			this.props.productStore!.products !== undefined ? (
				this.props.productStore!.products.map(p => (
					<ProductComp product={p} key={p.id} />
				))
			) : (
				<React.Fragment />
			);
		const cartItems = this.props.cartStore!.items.map(i => (
			<tr key={i.product.id}>
				<td>{i.product.name}</td>
				<td>{i.amount}</td>
				<td>{(i.product.price * i.amount).toLocaleString("de-DE")} €</td>
			</tr>
		));

		return (
			<div className="app-shop container-fluid">
				<div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
					<Logo className="logo" />
					<h5 className="my-0 mr-md-auto font-weight-light brand-name">
						<span className="font-weight-normal">
							HTW Microservices
						</span>{" "}
						Shop
					</h5>
					<Nav className="my-2 my-md-0 mr-md-3">
						<Nav.Item>
							<Nav.Link className="p-2 text-dark" href="#">
								Home
							</Nav.Link>
						</Nav.Item>
					</Nav>
					<Button
						variant="primary"
						onClick={() => this.props.authStore!.logOut()}>
						Abmelden
					</Button>
				</div>

				<div className="row">
					<div className="main col-lg-8">
						<div className="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
							<h1 className="display-4">
								{this.getGreeting()},{" "}
								{
									this.props.authStore!.loginState.customer!
										.firstName
								}
							</h1>
							<p className="lead">
								Sieh Dich gerne in unserem umfangreichen Angebot
								von außergewöhnlichen Produkten um:
							</p>
						</div>

						<div className="container">
							<div className="card-deck mb-3 text-center">
								{products}
							</div>
						</div>
						<div className="cart text-center col-md-6 offset-md-3 col-sm-12">
							Deine gewählten Artikel:
							<table>
								<tbody>
									<tr>
										<th>Artikel</th>
										<th>Anzahl</th>
										<th>Gesamt</th>
									</tr>
									{cartItems}
									<tr className="cart-sum">
										<td>Summe</td>
										<td />
										<td>
											{this.props.cartStore!.getTotal().toLocaleString("de-DE")} €
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<Button
							variant="primary"
							onClick={() =>
								orderService.sendOrder(
									this.props.cartStore!.items
								)
							}>
							Jetzt bestellen
						</Button>
					</div>
					<div className="notifications col-lg-4">
						<div className="notifications-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
							<h1 className="display-4">Benachrichtungen</h1>							
							<p className="lead">
								Hier siehst du deine eingehenden Benachrichtungen
							</p>
						</div>
						<Notifications />
					</div>
				</div>
				<p className="mt-5 mb-3 text-muted">
					&copy; 2019 Florian Hiensch
				</p>
			</div>
		);
	}
}

export default Shop;
