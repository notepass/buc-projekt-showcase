import * as React from "react";
import { inject, observer } from "mobx-react";
import Stores from "../../stores/store-identifier";
import NotificationStore from "../../stores/notification/notification-store";

export interface INotificationsProps {
	notificationStore?: NotificationStore;
}

export interface INotificationsState {}

@inject(Stores.NotificationStore)
@observer
class Notifications extends React.Component<
	INotificationsProps,
	INotificationsState
> {
	async componentDidMount() {
		await this.props.notificationStore!.getAllNotifications();
	}

	render() {
		const notifications = this.props
		//@ts-ignore
			.notificationStore!.notifications.sort((a, b) => new Date(b.date) - new Date(a.date))
			.map(n => (
				<div className="card" key={n.id}>
					<div className="card-body">
						<h5 className="card-title">{n.title}</h5>
						<h6 className="card-subtitle mb-2 text-muted">
							{new Date(n.date).toLocaleString("de-DE")}
						</h6>
						<p className="card-text">{n.body}</p>
					</div>
				</div>
			));
		return <div className="app-notifications">{notifications}</div>;
	}
}

export default Notifications;
