<?php
ob_start();
require_once 'sqlConnector.php';
require_once 'ConductoConnector.php';
//startShopWorkflow("1", "5", "1");
if (!isset($_COOKIE['userId'])) {
    header('Location: /index.php');
    ob_end_flush();
    exit;
}
?>

    <html>
    <head>
        <title>BUC Webshop</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>
            .shop_item {
                padding: 3%;
                border: 1px solid #ABABAB;
                border-radius: 3px;
                box-sizing: border-box;
            }
        </style>
    </head>

    <body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <div class="collapse navbar-collapse" id="main-navigation">
            <a class="navbar-brand" href="#">BUC Webshop</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Login/Logout</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/shop.php">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/manage.php">Bestellübersicht</a>
                </li>
                <li class="nav-item">
                    <a target="_blank" class="nav-link" href="https://gitlab.com/notepass/buc-projekt-showcase">GitLab-Projekt</a>
                </li>
            </ul>
        </div>
    </nav>
    <br/>
    <div class="container">
        <?php
        if (isset($_POST['buyProduct'])) {
            $amount = $_POST['productAmount'];
            if ($amount <= 0) {
                echo '<div class="alert alert-error">
                Du kannst nicht weniger als einen Artikel kaufen!
            </div>';
            } else {
                $id = $_POST['productId'];
                $userId = $_COOKIE['userId'];

                startShopWorkflow($id, $amount, $userId);

                echo '<div class="alert alert-info">
                Deine Bestellung ist eingegangen. Du kannst diese in der <a href="/manage.php">Bestellübersicht</a> verwalten.
            </div>';
            }

        }
        ?>

        <div class="row">
            <?php
            foreach (getAllArticles() as $article) {
                ?>
                <div class="col">
                    <div class="shop_item">
                        <p class="font-weight-bold"><?php echo $article->name ?></p>
                        <p class="font-weight-light">
                            <?php echo $article->description ?>
                        </p>
                        <p>Verfügbar: <?php echo $article->stock ?> Stück<br/>
                            Stückpreis: <?php echo $article->price ?> €</p>
                        <form action="/shop.php" method="post">
                            <div class="form-group">
                                <label for="productAmount">Anzahl:</label>
                                <input min="1" type="number" class="form-control" id="productAmount"
                                       name="productAmount">
                            </div>
                            <input type="hidden" name="productId" value="<?php echo $article->id ?>">
                            <button type="submit" class="btn btn-primary" name="buyProduct">Kaufen</button>
                        </form>
                    </div>
                </div>
                <?php
            }
            ?>
            <!--<div class="col">
                <div class="shop_item">
                    <p class="font-weight-bold">3D Mops</p>
                    <p class="font-weight-light">
                        Beschreibung<br/>
                        Mehr Beschreibung
                    </p>
                    <p>Verfügbar: 8 Stück<br/>
                        Stückpreis: 3.99 €</p>
                    <form action="/shop.php" method="post">
                        <div class="form-group">
                            <label for="productAmount">Anzahl:</label>
                            <input min="1" type="number" class="form-control" id="productAmount" name="productAmount">
                        </div>
                        <input type="hidden" name="productId" value="1">
                        <button type="submit" class="btn btn-primary" name="logMeIn">Kaufen</button>
                    </form>
                </div>
            </div>
            <div class="col">
                <div class="shop_item">
                    ITEM
                </div>
            </div>
            <div class="col">
                <div class="shop_item">
                    ITEM
                </div>
            </div>-->
        </div>
    </div>
    </body>
    </html>
<?php
ob_end_flush();
?>