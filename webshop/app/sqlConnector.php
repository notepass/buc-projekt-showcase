<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 06.06.2019
 * Time: 03:02
 */

class User {
    public function __construct($first, $last, $idd)
    {
        $this->firstName=$first;
        $this->lastName=$last;
        $this->userId=$idd;
    }

    public $firstName;
    public $lastName;
    public $userId;
}

class Article {


    public $id;
    public $name;
    public $stock;
    public $description;
    public $price;

    /**
     * Article constructor.
     * @param $id
     * @param $name
     * @param $stock
     * @param $description
     * @param $price
     */
    public function __construct($id, $name, $stock, $description, $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->stock = $stock;
        $this->description = $description;
        $this->price = $price;
    }
}

class Order {


    public $id;
    public $article;
    public $amount;
    public $statusLabel;

    /**
     * Order constructor.
     * @param $id
     * @param $article
     * @param $amount
     */
    public function __construct($id, $article, $amount, $status)
    {
        $this->id = $id;
        $this->article = $article;
        $this->amount = $amount;
        switch ($status) {
            case "0":
                $this->statusLabel="Unbearbeitet";
                break;
            case "1":
                $this->statusLabel="In Bearbeitung";
                break;
            case "2":
                $this->statusLabel="Storniert: Keine zahlung erhalten";
                break;
            case "3":
                $this->statusLabel="Storniert: Ungenügender Lagerbestand";
                break;
        }
    }
}

function doSql($sql, $args) {
    $db = new PDO('mysql:host='.getenv("SQL_HOST").';dbname=buc_webshop;charset=utf8', getenv("SQL_USERNAME"), getenv("SQL_PASSWORD") );
    $stmt=$db->prepare($sql);
    $stmt->execute($args);
    return $stmt;
}

function getUserByName($firstName, $lastName) {
    $stmt=doSql("SELECT * FROM users WHERE firstName = ? AND lastName = ?", array($firstName, $lastName));

    foreach ($stmt as $item) {
        return new User($item['firstName'], $item['lastName'], $item['id']);
    }

    doSql("INSERT INTO users (firstName, lastName) VALUES (?, ?)", array($firstName, $lastName));
    return getUserByName($firstName, $lastName);
}

function getAllArticles() {
    $res = array();

    $stmt=doSql("SELECT * FROM products", array());

    foreach ($stmt as $item) {
        array_push($res, new Article($item['id'], $item['name'], $item['stock'], $item['description'], $item['price']));
    }

    return $res;
}

function getAllOrdersForUser($userId) {
    $stmt = doSql("SELECT * FROM orders JOIN orders_product_map ON (orders.id = orders_product_map.orderId) WHERE userId = ?", array($userId));

    $res = array();

    foreach ($stmt as $item) {
        foreach (getAllArticles() as $article) {
            if ($article->id == $item['productId']) {
                $art = $article;
            }
        }

        array_push($res, new Order($item['orderId'], $art, $item['amount'], $item['status']));

        return $res;
    }
}

function getOrder($orderId, $userId) {
    foreach (getAllOrdersForUser($userId) as $order) {
        if ($order->id == $orderId) {
            return $order;
        }
    }

    return false;
}

function getMessages($orderId) {
    $stmt=doSql("SELECT * FROM messages WHERE orderId = ?", array($orderId));

    $res = array();

    foreach ($stmt as $msg) {
        array_push($res, $msg['content']);
    }

    return $res;
}


?>