<?php
ob_start();
require_once 'sqlConnector.php';
require_once 'ConductoConnector.php';
//startShopWorkflow("1", "5", "1");
if (!isset($_COOKIE['userId'])) {
    header('Location: /index.php');
    ob_end_flush();
    exit;
}
?>

    <html>
    <head>
        <title>BUC Webshop</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>
            .shop_item {
                padding: 3%;
                border: 1px solid #ABABAB;
                border-radius: 3px;
                box-sizing: border-box;
            }
        </style>
    </head>

    <body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <div class="collapse navbar-collapse" id="main-navigation">
            <a class="navbar-brand" href="#">BUC Webshop</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Login/Logout</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/shop.php">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/manage.php">Bestellübersicht</a>
                </li>
                <li class="nav-item">
                    <a target="_blank" class="nav-link" href="https://gitlab.com/notepass/buc-projekt-showcase">GitLab-Projekt</a>
                </li>
            </ul>
        </div>
    </nav>
    <br/>
    <div class="container">
        <?php
        if (isset($_GET['orderId'])) {
            $msgs = getMessages($_GET['orderId']);

            if (sizeof($msgs) <= 0) {
                echo "<div class=\"alert alert-warning\">
                Die angegebene Bestellung kann nicht gefunden werden
            </div>";
            } else {

            ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nachricht</th>
                </tr>
                </thead>
                <tbody>

                <?php
                foreach ($msgs as $msg) {
                    ?>
                    <tr>
                        <td><?php echo $msg ?></td>
                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table><br/>
                <a href="manage.php">Zurück</a>
            <?php
        }
        } else {
            ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Bestellung</th>
                    <th>Status</th>
                    <th>Weitere Informationen</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $orders = getAllOrdersForUser($_COOKIE['userId']);
                if (sizeof($orders) > 0) {
                    foreach ($orders as $order) {
                        ?>
                        <tr>
                            <td><?php echo $order->amount ?> x <?php echo $order->article->name ?></td>
                            <td><?php echo $order->statusLabel ?></td>
                            <td><a href="manage.php?orderId=<?php echo $order->id ?>">Weitere Informationen</a></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
            <?php
        }
        ?>
    </div>
    </body>
    </html>
<?php
ob_end_flush();
?>