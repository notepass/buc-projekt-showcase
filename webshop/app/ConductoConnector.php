<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 06.06.2019
 * Time: 04:19
 */

function startShopWorkflow($productId, $productAmount, $userId) {
    httpPost(getenv("CONDUCTOR_API_WF_URL"), '{
"userId": '.$userId.',
"productId": '.$productId.',
"productAmount": '.$productAmount.'
}');
}

function httpPost($url, $data)
{
    //echo "Post: ||$url||$data";
    /*
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    //print_r($response);
    return $response;
    */

    /*
    echo "Strat shell exec";
    echo shell_exec('/usr/bin/curl -X POST "'.$url.'" -H \'Content-Type: application/json\' -d \''.$data."''");
    echo "End shell exec";
    echo "Strat exec";
    $a = array();
    echo exec('/usr/bin/curl -X POST "'.$url.'" -H \'Content-Type: application/json\' -d \''.$data."''", $a);
    print_r($a);
    echo "End exec";
    */

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    //print_r($response);
    curl_close($curl);
    return $response;
}