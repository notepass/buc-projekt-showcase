<?php
ob_start();
?>
    <html>
    <head>
        <title>BUC Webshop</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <div class="collapse navbar-collapse" id="main-navigation">
            <a class="navbar-brand" href="#">BUC Webshop</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Login/Logout</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/shop.php">Shop</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/manage.php">Bestellübersicht</a>
                </li>
                <li class="nav-item">
                    <a target="_blank" class="nav-link" href="https://gitlab.com/notepass/buc-projekt-showcase">GitLab-Projekt</a>
                </li>
            </ul>
        </div>
    </nav><br/>
    <div class="container">
        <?php
        require_once 'sqlConnector.php';

        // ======================================================================================
        if (!isset($_COOKIE['userId']) && !isset($_POST['logMeIn'])) {
            ?>
            <div class="alert alert-warning">
                Bitte Logge dich ein um den Webshop nutzen zu können
            </div>

            <form action="/index.php" method="post">
                <div class="form-group">
                    <label for="firstName">Vorname:</label>
                    <input type="text" class="form-control" id="firstName" name="firstName">
                </div>
                <div class="form-group">
                    <label for="lastName">Nachname:</label>
                    <input type="text" class="form-control" id="lastName" name="lastName">
                </div>
                <button type="submit" class="btn btn-primary" name="logMeIn">Login</button>
            </form>
            <?php
        }
        // ======================================================================================
        ?>

        <?php
        // =======================================================================================
        if (!isset($_COOKIE['userId'])
            && isset($_POST['logMeIn'])
            && isset($_POST['firstName'])
            && isset($_POST['lastName'])
            && $_POST['firstName'] != ''
            && $_POST['lastName'] != '') {

            $userId = getUserByName($_POST['firstName'], $_POST['lastName'])->userId;
            setcookie("userId", $userId);

            ?>
            Login erfolgreich. <a href="/shop.php">Zum Shop</a>

            <?php
        } elseif (isset($_POST['logMeIn'])) {
            echo "Bitte gebe einen Vor- und Nachnamen an<br/><a href='index.php'>Zur&uuml;ck</a> ";
        }
        // =======================================================================================
        ?>
    </div>
    </body>
    </html>
<?php
ob_end_flush();
?>