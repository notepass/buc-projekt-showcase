package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShippingServiceWorker implements Worker {
    private static final Logger LOGGER = LogManager.getLogger(ShippingServiceWorker.class);

    public String getTaskDefName() {
        return "shipping_service";
    }

    public TaskResult execute(Task task) {
        String message = "Ihre Bestellung wurde Versand. Das Packet sollte innerhalb von 3 bis 6 Wertagen bei ihnen eintreffen.";
        TaskResult result = ConductorUtils.prepareTaskResult(task);
        result.getOutputData().put("message_content", message);

        try {
            SqlDao.updateOrder((Integer) task.getInputData().get("orderId"), SqlDao.ORDER_STATUS_SHIPPED);
        } catch (Exception e) {
            LOGGER.error("Could not process shipping:", e);
            result.setStatus(TaskResult.Status.FAILED);
        }

        return result;
    }
}
