package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SimpleWorker implements Worker {
    private static final Logger LOGGER = LogManager.getLogger(SimpleWorker.class);

    public String getTaskDefName() {
        return "simple_test_task";
    }

    public TaskResult execute(Task task) {
        LOGGER.info("SimpleWorker called");
        TaskResult result = new TaskResult(task);

        if (task.getInputData().get("key1") != null) {
            result.getOutputData().put("key1null", true);
        } else {
            result.getOutputData().put("key1null", false);
        }


        result.setStatus(TaskResult.Status.COMPLETED);
        return result;
    }
}
