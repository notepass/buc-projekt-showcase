package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.http.TaskClient;
import com.netflix.conductor.client.task.WorkflowTaskCoordinator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException {
        int numSysThreads = 2;

        LOGGER.info("Setting up client");
        TaskClient client = new TaskClient();
        client.setRootURI(System.getenv("CONDUCTOR_API_URL"));

        LOGGER.info("Initializing worker");
        SimpleWorker worker = new SimpleWorker();

        LOGGER.info("Initialing WorkflowTaskCoordinator");
        WorkflowTaskCoordinator.Builder builder = new WorkflowTaskCoordinator.Builder();
        WorkflowTaskCoordinator coordinator = builder.withWorkers(worker).withThreadCount(numSysThreads).withTaskClient(client).build();

        LOGGER.info("Starting polling");
        coordinator.init();
    }
}
