#!/usr/bin/env bash

CONDUCTOR_API_URL="http://192.168.99.100:8080/api"
echo $CONDUCTOR_API_URL/metadata/taskdefs
curl -X POST \
  $CONDUCTOR_API_URL/metadata/taskdefs \
  -H 'Content-Type: application/json' \
  -d '[
    {
  "name": "simple_test_task",
  "retryCount": 3,

  "inputKeys": [
    "key1"
  ],
  "outputKeys": [
    "key1null"
  ],
  "timeoutPolicy": "TIME_OUT_WF",
  "retryLogic": "FIXED"
}
]'

curl -X POST \
  $CONDUCTOR_API_URL/metadata/workflow \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "simple_test_workflow",
  "description": "Encodes a file and deploys to CDN",
  "version": 1,
  "tasks": [
  {
    "name": "simple_test_task",
    "taskReferenceName": "simple_test_task",
    "type": "SIMPLE",
    "inputParameters": {
      "key1": "${workflow.input.key1}"
    }
  }
  ],
  "outputParameters": {
    "key1null": "${workflow.output.key1null}"
  },
  "restartable": true,
  "workflowStatusListenerEnabled": true,
  "schemaVersion": 2
}'

curl -X POST \
  $CONDUCTOR_API_URL/workflow/simple_test_workflow \
  -H 'Content-Type: application/json' \
  -d '{
    "key1": "something"
}'