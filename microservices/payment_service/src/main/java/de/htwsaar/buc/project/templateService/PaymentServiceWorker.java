package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class PaymentServiceWorker implements Worker {
    private static final Logger LOGGER = LogManager.getLogger(PaymentServiceWorker.class);

    public String getTaskDefName() {
        return "payment_check_service";
    }

    public TaskResult execute(Task task) {
        TaskResult result = ConductorUtils.prepareTaskResult(task);
        String message;

        try {
            boolean paymentOk = new Random().nextBoolean();
            if (paymentOk) {
                message = "Ihre Zahlung wurde Erhalten. Wir werden die Bestellung frühstmöglich versenden.";
                result.getOutputData().put("paymentOk", true);
            } else {
                //TODO: Add back stock
                message = "Leider wurde keine Zahlung für die Bestellung Erhalten. Ihre bestellung wurde Storniert.";
                SqlDao.updateOrder((Integer) task.getInputData().get("orderId"), SqlDao.ORDER_STATUS_MISSING_PAYMENT);
                result.getOutputData().put("paymentOk", false);
            }

            result.getOutputData().put("message_content", message);

        } catch (Exception e) {
            LOGGER.error("Error while checking for payment: ", e);
            result.setStatus(TaskResult.Status.FAILED);
        }

        return result;
    }
}
