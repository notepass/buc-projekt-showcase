package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class StockCheckerService implements Worker {
    private static final Logger LOGGER = LogManager.getLogger(StockCheckerService.class);

    public String getTaskDefName() {
        return "stock_checker_service";
    }

    public TaskResult execute(Task task) {
        TaskResult result = ConductorUtils.prepareTaskResult(task);

        try {
            String message;

            int productId = (Integer) task.getInputData().get("productId");
            int orderedAmount = (Integer) task.getInputData().get("productAmount");

            Map<String, String> info = SqlDao.getProductInformation(productId);
            int stockAmount = Integer.parseInt(info.get("productStock"));

            if (stockAmount < orderedAmount) {
                message = "Leider können wir Ihre Bestellung nicht ausführen, da der von ihnen Bestellte Artikel nicht mehr auf Lager ist.";
                SqlDao.updateOrder((Integer) task.getInputData().get("productAmount"), SqlDao.ORDER_STATUS_MISSING_STOCK);
                result.getOutputData().put("stockAmountOk", false);
            } else {
                int remainingStock = stockAmount - orderedAmount;
                if (remainingStock < 0) {
                    remainingStock = 0;
                }
                SqlDao.updateProductStock(productId, remainingStock);
                message = "Ihre Bestellung ist eingegangen. Sobald die Zahlung erfolgt ist, werden wir die Artikel versenden.";
                result.getOutputData().put("stockAmountOk", true);
            }

            result.getOutputData().put("message_content", message);
        } catch (Exception e) {
            LOGGER.error("Error while processing stock checking:", e);
            result.setStatus(TaskResult.Status.FAILED);
        }

        return result;
    }
}
