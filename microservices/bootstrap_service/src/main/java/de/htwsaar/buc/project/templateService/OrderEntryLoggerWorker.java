package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class OrderEntryLoggerWorker implements Worker {
    private static final Logger LOGGER = LogManager.getLogger(OrderEntryLoggerWorker.class);

    public String getTaskDefName() {
        return "order_entry_logger";
    }

    public TaskResult execute(Task task) {
        TaskResult result = ConductorUtils.prepareTaskResult(task);

        try {
            Map<String, String> userData = SqlDao.getUserInfo((Integer) task.getInputData().get("userId"));
            Map<String, String> productData = SqlDao.getProductInformation((Integer) task.getInputData().get("productId"));
            String name = userData.get("userFirstName") + " " + userData.get("userLastName");

            LOGGER.info("Bestellung erhalten von " + name + ": " + task.getInputData().get("productAmount") + " mal " + productData.get("productName"));

            int orderId = SqlDao.createOrder(
                    (Integer) task.getInputData().get("userId"),
                    (Integer) task.getInputData().get("productId"),
                    (Integer) task.getInputData().get("productAmount")
            );

            result.getOutputData().put("orderId", orderId);
            result.setStatus(TaskResult.Status.COMPLETED);
        } catch (Exception e) {
            LOGGER.error("Error while creating order: ", e);
            result.setStatus(TaskResult.Status.FAILED_WITH_TERMINAL_ERROR);
        }
        return result;
    }
}
