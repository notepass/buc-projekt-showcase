package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserInformationWorker implements Worker {
    private static final Logger LOGGER = LogManager.getLogger(UserInformationWorker.class);

    public String getTaskDefName() {
        return "user_information_service";
    }

    public TaskResult execute(Task task) {
        TaskResult result = ConductorUtils.prepareTaskResult(task);
        try {
            result.getOutputData().remove("messageContent");
            int orderId = (Integer) task.getInputData().get("orderId");

            SqlDao.addMessage(orderId, "", (String) task.getInputData().get("messageContent"));
        } catch (Exception e) {
            LOGGER.error("Could not add message: ",e);
            result.setStatus(TaskResult.Status.FAILED);
        }

        return result;
    }
}
