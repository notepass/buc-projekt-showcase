package de.htwsaar.buc.project.templateService;

import com.netflix.conductor.client.http.TaskClient;
import com.netflix.conductor.client.task.WorkflowTaskCoordinator;
import com.netflix.conductor.client.worker.Worker;
import com.netflix.conductor.common.metadata.tasks.Task;
import com.netflix.conductor.common.metadata.tasks.TaskResult;
import org.apache.http.conn.HttpInetSocketAddress;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.net.Socket;

public class ConductorUtils {
    private static final Logger LOGGER = LogManager.getLogger(ConductorUtils.class);

    public static void registerWorkers(Worker ... workers) {
        boolean ok = false;

        while (!ok) {
            try (Socket s = new Socket("conductor-server", 8080)) {
                ok = true;
            } catch (Exception e) {

            } finally {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        int numSysThreads = 2;

        LOGGER.info("Setting up client");
        TaskClient client = new TaskClient();
        client.setRootURI(System.getenv("CONDUCTOR_API_URL"));

        LOGGER.info("Initialing WorkflowTaskCoordinator");
        WorkflowTaskCoordinator.Builder builder = new WorkflowTaskCoordinator.Builder();
        WorkflowTaskCoordinator coordinator = builder.withWorkers(workers).withThreadCount(numSysThreads).withTaskClient(client).build();

        LOGGER.info("Starting polling");
        coordinator.init();
    }

    public static TaskResult prepareTaskResult(Task task) {
        //Add time delay for showcase purpose
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            //Ignore
        }

        TaskResult result = new TaskResult(task);
        for (String key:task.getInputData().keySet()) {
            result.getOutputData().put(key, task.getInputData().get(key));
        }

        result.setStatus(TaskResult.Status.COMPLETED);
        return result;
    }
}
