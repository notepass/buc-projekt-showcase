package de.htwsaar.buc.project.templateService;

import java.sql.*;

public class SqlUtil {
    private static Connection con = null;

    public static synchronized PreparedStatement createPreparedStatement(String stmt) throws SQLException {
        //String url = "jdbc:mysql://localhost/Personal";
        //Class.forName("com.mysql.jdbc.Driver");
        if (con == null) {
            if (System.getenv("SQL_PASSWORD") == null || System.getenv("SQL_PASSWORD").isEmpty()) {
                con = DriverManager.getConnection(
                        System.getenv("SQL_CONNECTION_STRING"),
                        System.getenv("SQL_USERNAME"),
                        null
                );
            } else {
                con = DriverManager.getConnection(
                        System.getenv("SQL_CONNECTION_STRING"),
                        System.getenv("SQL_USERNAME"),
                        System.getenv("SQL_PASSWORD")
                );
            }
        }
        return con.prepareStatement(stmt);

        /*
        PreparedStatement ps = conn.prepareStatement("SELECT Wohnort, Strasse FROM Angestellte WHERE Vorname=? AND Nachname=?");
        ps.setString(1, "Willi");
        ps.setString(2, "Meier");
        ResultSet rs = ps.executeQuery();
        while (rs.next())
            System.out.println(rs.getString(1) + "\n" + rs.getString(2));
        ps.close();
        conn.close();
        */
    }
}
