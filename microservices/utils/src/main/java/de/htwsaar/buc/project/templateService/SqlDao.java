package de.htwsaar.buc.project.templateService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlDao {
    public static final int ORDER_STATUS_NEW = 0;
    public static final int ORDER_STATUS_SHIPPED = 1;
    public static final int ORDER_STATUS_MISSING_PAYMENT = 2;
    public static final int ORDER_STATUS_MISSING_STOCK = 3;

    public static Map<String, String> getUserInfo(int userId) throws SQLException {
        PreparedStatement stmt = SqlUtil.createPreparedStatement("SELECT * FROM users WHERE id = ?");
        stmt.setInt(1, userId);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Map<String, String> out = new HashMap<String, String>();
            out.put("userFirstName", rs.getString("firstName"));
            out.put("userLastName", rs.getString("lastName"));
            return out;
        }

        return new HashMap<String, String>();
    }

    public static Map<String, String> getProductInformation(int productId) throws SQLException {
        PreparedStatement stmt = SqlUtil.createPreparedStatement("SELECT * FROM products WHERE id = ?");
        stmt.setInt(1, productId);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Map<String, String> out = new HashMap<String, String>();
            out.put("productName", rs.getString("name"));
            out.put("productStock", rs.getString("stock"));
            out.put("productDescription", rs.getString("description"));
            out.put("productPrice", rs.getString("price"));
            return out;
        }

        return new HashMap<String, String>();
    }

    public static void updateProductStock(int productId, int newStockAmount) throws SQLException {
        PreparedStatement stmt = SqlUtil.createPreparedStatement("UPDATE products SET stock = ? WHERE id = ?");
        stmt.setInt(1, newStockAmount);
        stmt.setInt(2, productId);
        stmt.execute();
    }

    public static void addMessage(int orderId, String title, String content) throws SQLException {
        PreparedStatement stmt = SqlUtil.createPreparedStatement("INSERT INTO messages (orderId, content, title) VALUES (?, ?, ?)");
        stmt.setInt(1, orderId);
        stmt.setString(2, content);
        stmt.setString(3, title);

        stmt.execute();
    }

    public static int createOrder(int userId, int productId, int productAmount) throws SQLException {
        PreparedStatement stmt = SqlUtil.createPreparedStatement("INSERT INTO orders (userId, status) VALUES (?, ?)");
        stmt.setInt(1, userId);
        stmt.setInt(2, ORDER_STATUS_NEW);
        stmt.execute();
        stmt = SqlUtil.createPreparedStatement("SELECT * FROM orders WHERE userId = ? AND status = 0");
        stmt.setInt(1, userId);
        ResultSet rs = stmt.executeQuery();
        int orderId = -1;
        while (rs.next()) {
            orderId = rs.getInt("id");
        }

        PreparedStatement stmt2 = SqlUtil.createPreparedStatement("INSERT INTO orders_product_map (orderId, productId, amount) VALUES (?, ?, ?)");
        stmt2.setInt(1, orderId);
        stmt2.setInt(2, productId);
        stmt2.setInt(3, productAmount);
        stmt2.execute();

        return orderId;
    }

    public static void updateOrder(int orderId, int newStatus) throws SQLException {
        PreparedStatement stmt = SqlUtil.createPreparedStatement("UPDATE orders SET status = ? WHERE id = ?");
        stmt.setInt(1, newStatus);
        stmt.setInt(2, orderId);

        stmt.execute();
    }
}
